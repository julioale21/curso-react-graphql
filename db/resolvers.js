const Usuario = require('../models/Usuario');
const Producto = require('../models/Producto');
const Cliente = require('../models/Cliente');
const Pedido = require('../models/Pedido');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config({ path: 'variables.env' });

const crearToken = (usuario, secreta, expiresIn) => {
    const { id, email, nombre, apellido } = usuario;
    return jwt.sign({ id, email, nombre, apellido }, secreta, { expiresIn })
}

const resolvers = {
    Query: {
        obtenerUsuario: async (_, {}, ctx) => {
            return ctx.usuario;
        },
        obtenerProductos: async () => {
            try {
                const productos = Producto.find({});
                return productos;
            } catch (error) {
                console.log(error);
            }
        },
        obtenerProducto: async (_, { id }) => {
            const producto = await Producto.findById(id);

            if(!producto) throw new Error('Producto no encontrado');

            return producto;
        },

        obtenerClientes: async () => {
            try {
                const clientes = await Cliente.find({});
                return clientes;
            } catch (error) {
                console.log(error);
            }
        },

        obtenerClientesVendedor: async (_, {}, ctx ) => {
            try {
                const clientes = await Cliente.find({ vendedor: ctx.usuario.id.toString() });
                return clientes;
            } catch (error) {
                console.log(error);
            }
        },

        obtenerCliente: async (_, { id }, ctx) => {
            // Revisar si el cliente existe
            const cliente = await Cliente.findById(id);
            if (!cliente) {
                throw new Error('Cliente no encontrado');
            }

            // Quien lo creo puede verlo
            if (cliente.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No posee las credenciales necesarias');
            }

            return cliente;
        },

        obtenerPedidos: async () => {
            try {
                const pedidos = await Pedido.find({});
                return pedidos;
            } catch (error) {
                console.log(error);
            }   
        },

        obtenerPedidosVendedor: async ( _ , {}, ctx ) => {
            try {
                const pedidos = await Pedido.find({ vendedor: ctx.usuario.id }).populate('cliente');
                return pedidos;
            } catch (error) {
                console.log(error);
            }
        },

        obtenerPedido: async (_, {id}, ctx) => {
            const pedido = await Pedido.findById(id);
            if (!pedido) {
                throw new Error('El pedido no existe');
            }

            if(pedido.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No tiene permisos suficientes');
            }
            return pedido;
        },

        obtenerPedidosEstado: async (_, { estado }, ctx ) => {
            const pedidos = await Pedido.find({ vendedor: ctx.usuario.id, estado});
            return pedidos;
        },

        mejoresClientes: async () => {
            const clientes = await Pedido.aggregate([
                { $match: { estado: "COMPLETADO" } },
                { $group: {
                    _id: "$cliente",
                    total: { $sum: '$total'}
                }},
                {
                    $lookup: {
                        from: 'clientes',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'cliente'
                    }
                },
                {
                    $limit: 10
                },
                {
                    $sort: { total: -1 }
                }
            ]);

            return clientes;
        },

        mejoresVendedores: async () => {
            const vendedores = await Pedido.aggregate([
                { $match: { estado: 'COMPLETADO' } },
                { $group: {
                    _id: '$vendedor',
                    total: { $sum: '$total' }
                }},
                { 
                    $lookup: {
                        from: 'usuarios',
                        localField: '_id',
                        foreignField: '_id',
                        as: 'vendedor'
                    }
                },
                {
                    $limit: 3
                },
                {
                    $sort: { total: -1 }
                }
            ]);

            return vendedores;
        },

        buscarProducto: async (_, { texto }) => {
            const productos = await Producto.find({ $text: { $search: texto }}).limit(10);
            return productos;
        }
    },


    Mutation: {
        nuevoUsuario: async (_, { input }) => {

            const { email, password } = input;
            
            // Revisar si el usuario ya esta registrado
            const existeUsuario = await Usuario.findOne({email});
            if(existeUsuario) {
                throw new Error('El usuario ya esta registrado')
            }
    
            // Hashear su password
            const salt = bcrypt.genSaltSync(10);
            input.password = bcrypt.hashSync(password, salt);

            try {
                const usuario = new Usuario(input);
                usuario.save();
                return usuario;
            } catch (error) {
                console.log(error);
            }
        },

        autenticarUsuario: async (_, { input }) => {
            const { email, password } = input;

            // Si el usuario existe
            const existeUsuario = await Usuario.findOne({ email });
            if (!existeUsuario) {
                throw new Error('El usuario no existe');
            }

            // Revisar si el password es correcto
            const passwordCorrecto = await bcrypt.compare( password, existeUsuario.password );

            if (! passwordCorrecto) {
                throw new Error('La contraseña es incorrecta.');
            }

            // Crear el token
            return {
                token: crearToken(existeUsuario, process.env.SECRETA, '24h')
            }
        },

        nuevoProducto: async (_, { input }) => {
            try {
                const producto = new Producto(input);
                const resultado = await producto.save();
                return resultado;
            } catch (error) {
                console.log(error);
            } 
        },

        actualizarProducto: async (_, { id, input }) => {

            let producto = await Producto.findById(id);

            if(!producto) throw new Error('Producto no encontrado');
            producto = await Producto.findByIdAndUpdate({ _id: id }, input, { new: true });
            return producto;
        },

        eliminarProducto: async ( _, { id } ) => {

            let producto = await Producto.findById(id);

            if(!producto) throw new Error('Producto no encontrado');
            await Producto.findOneAndRemove({ _id: id });
            return 'Producto eliminado correctamente';
        },

        nuevoCliente: async (_, { input }, context ) => {
            const { email } = input;
            const cliente = await Cliente.findOne({ email });

            if(cliente) throw new Error('El cliente ya se encuentra registrado');
            try {
                const nuevoCliente = new Cliente(input);
                nuevoCliente.vendedor = context.usuario.id;
                const resultado = await nuevoCliente.save();
                return resultado;
            } catch (error) {
                console.log(error);
            }
        },

        actualizarCliente: async (_, { id, input }, ctx) => {
            let cliente = await Cliente.findById(id);

            if(!cliente) {
                throw new Error('El cliente no existe');
            }

            if(cliente.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No posee permisos suficientes');
            }

            cliente = await Cliente.findOneAndUpdate({_id: id}, input, {new: true});
            return cliente;
        },

        eliminarCliente: async (_, {id}, ctx) => {
            let cliente = await Cliente.findById(id);

            if(!cliente) {
                throw new Error('El cliente no existe');
            }

            if(cliente.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No posee los permisos suficientes');
            }

            await Cliente.findOneAndDelete({_id: id});
            return 'Cliente eliminado exitosamente';
        },

        nuevoPedido: async (_, {input}, ctx) => {
            const { cliente: id } = input

            const cliente = await Cliente.findById(id);
            if (!cliente) {
                throw new Error('El cliente no existe')
            }

            if (cliente.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No tiene los permisos suficientes');
            }
            
            for await ( const articulo of input.pedido) {
                const { id } = articulo;

                const producto = await Producto.findById(id);
                if (articulo.cantidad > producto.existencia ) {
                    throw new Error(`El articulo ${producto.nombre} excede la cantidad disponible`);
                } else {
                    producto.existencia = producto.existencia - articulo.cantidad;
                    await producto.save();
                }
            }

            const nuevoPedido = new Pedido(input);
            nuevoPedido.vendedor = ctx.usuario.id;
            const resultado = await nuevoPedido.save();
            return resultado;
        },

        actualizarPedido: async (_, { id, input }, ctx) => {
            const { cliente: idCliente } = input
            
            const pedido = await Pedido.findById(id);
            if(!pedido) {
                throw new Error('El pedido no existe');
            }

            const cliente = await Cliente.findById(idCliente);
            if(!cliente) {
                throw new Error('El cliente no existe');
            }

            if(cliente.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('No tiene los permisos suficientes');
            }
            
            if(input.pedido) {
                for await ( const articulo of input.pedido) {
                    const { id } = articulo;
    
                    const producto = await Producto.findById(id);
                    if (articulo.cantidad > producto.existencia ) {
                        throw new Error(`El articulo ${producto.nombre} excede la cantidad disponible`);
                    } else {
                        producto.existencia = producto.existencia - articulo.cantidad;
                        await producto.save();
                    }
                }
            }
            
            const resultado = await Pedido.findOneAndUpdate({ _id: id }, input, { new: true });
            return resultado;
        },

        eliminarPedido: async (_, {id}, ctx) => {
            const pedido = await Pedido.findById(id);
            if(!pedido) {
                throw new Error('El pedido no existe');
            }

            if(pedido.vendedor.toString() !== ctx.usuario.id) {
                throw new Error('Permisos insuficientes');
            }

            await Pedido.findOneAndDelete({ _id: id });
            return 'Pedido eliminado';
        }
    }
}

module.exports = resolvers;